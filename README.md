%%%
title = "Divide and map. Now."
%%%

# Divide and map. Now.
The [damn project][1] helps mappers by dividing some big area into smaller
squares that a human can map.

- The development happens on [GitLab][2].
- Chat on [Gitter][3].
- Summary and running instances on [OpenStreetMap wiki][4].
- News posted to [#damn][6] hashtag on Mastodon.
- Run your own instance with the [damn deploy][7] guide.

[1]: http://www.damn-project.org/
[2]: https://gitlab.com/damn-project
[3]: https://gitter.im/damn-project/community
[4]: https://wiki.openstreetmap.org/wiki/Divide_and_map._Now.
[6]: https://en.osm.town/web/timelines/tag/damn
[7]: https://gitlab.com/damn-project/damn_deploy/-/blob/master/README.md

## Diaries
- [Divide and map. Now.](https://www.openstreetmap.org/user/qeef/diary/391778)
- [Recent changes in the client](https://www.openstreetmap.org/user/qeef/diary/392186)
- [Deploy easily (in an hour)](https://www.openstreetmap.org/user/qeef/diary/392899)
- [Load testing](https://www.openstreetmap.org/user/qeef/diary/393240)

## Talks
- [FOSDEM 2020](https://fosdem.org/2020/schedule/event/geo_damn/)
  ([deck](https://qeef.gitlab.io/talks/damn-fosdem-2020/))
- [#WeMap session](https://youtu.be/XZiHSXCmL-s?t=327)
  ([deck](https://qeef.gitlab.io/talks/divide-and-wemap-now/))
- [SotM 2020](https://media.ccc.de/v/sotm2020-5031-lightning-talks-ii)
  ([deck](https://qeef.gitlab.io/talks/damn-lightning-talk/))

## Code of conduct
1. The community drives the damn project.
2. Become a good community member.
3. Learn to be efficient.
4. Help others.

### The community drives the damn project
The community discusses all the damn things. The community makes damn
decisions.

The community encourages split to smaller groups with narrower specialization.
It makes no sense to discuss JOSM client for mappers and a web client for
managers in the same group. The community encourages creating new clients of
all kinds.

### Become a good community member
Discuss. Try to be empathetic, respectful, patient. Try to discuss without
emotions.

### Learn to be efficient
Distinguish if you are talking about *what*, *why*, or *how*.

*How* is discussed only after *what* is agreed.

If you present *what*, be prepared to explain *why* in different points of view
tirelessly.

If you discuss *what* because you agree/disagree with it, try hard to
understand *why* and provide constructive feedback.

Don't forget to stick with developers guides (i.e., branching model, keeping a
changelog, etc.)

### Help others
Because it is nice, and it increases efficiency.

<center>*Keep mapping!*</center>
